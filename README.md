RE_LALE
===

#### Reverse engineering the first Turkish fantasy role-playing game

![](http://oi68.tinypic.com/ws1tt2.jpg) ![](http://oi68.tinypic.com/2mnn50l.jpg) ![](http://oi66.tinypic.com/eojlf.jpg)

This is a repository dedicated to reverse engineering the MS-DOS version of  _Lale Savascilari_; the first Turkish FRP game ever made.
I managed to figure out some of the file formats for game resources; and converted these resources to more accessible formats.
My initial objective is to organize what I obtained so far (which is decoding individual file formats and resources); then proceeding to reverse the binary itself. I'm not in a rush, though. Feel free to contribute.

#### The Game

> _Lale Savaşçıları (Tulip Warriors)_ is a computer game first developed in 1994 by SiliconWorx for the Amiga platform. Later it was re-produced for the PC platform by Compuphiliacs and SiliconWorx and distributed by Raks New Media in 1996. In addition to being one of the first PC and CD-ROM games in Turkey, it is still accepted as a major turning point in the history of Turkish gaming. Lale Savaşçıları can be classified as an RPG. As is customary in RPG games, it is based on using the different personal traits of multiple (i.e. four) characters correctly, and solving the puzzles that appear in the game intelligently (usually beating). In terms of playing style and interface it is similar to the average RPGs of its time. What makes it (in)famous however is its style and story.

#### Story

> "After the death of Ridvan and Barbaros, minions of darkness started conspiring to capture Istanbul, where a handful of watchmen of good, unaware of upcoming danger, are putting their hearts and souls into saving Istanbul but lacking the power. Mayhem reigns Istanbul; "Maganda"s (Turkish: Hick, brute...) are fighting against "lavuk"s (Turkish: a person who makes a fuss, who speaks too much and unnecessary); monsters of inflation, municipality, unemployment are starting to roam around once again. In all this chaos, 4 ordinary Istanbulites who got together incidentally, find themselves amidst in a grand conspiracy with the duty of uniting citizens of Istanbul. Will this 4 people; stop the forces of darkness, end the ignorance and prevent the civil war? Will the citizens of Istanbul, who never got along, find peace and harmony within themselves? Or will the dark forces, who make use of ignorance and naivety of people, overcome against you and defenders of light in this conspiracy over Istanbul?"

## Resources

Game resources are in the `res/` directory.

For any information regarding how to decode each file format; see `res/README.md`.

__


License
----

GPLv3.
