Decoding the resource files
===

The game comes with multiple archive files containing game resources such as music, images, AMOS banks migrated from the Amiga version of the game, etc. These archive files have a `.CKD` extension and they are under `res/ckd/`.

### .CKD files

A `.CKD` file is an uncompressed archive file for storing multiple files. Reversing the function that extracts these files revealed the following characteristics:

- Bytes [0, 4) contain the magic string `FLiB`.
- Bytes [8, 12) contain a little-endian integer `dirIdx`; indicating the offset of a **file table**.
- Bytes [12, 16) contain `FiLE` (start of the first file). Afterwards; the offset of each individual file also starts with `FiLE` as well. The names and start offsets of each file is described in the file table.

Each 65-byte block from offset `dirIdx` to the end of the archive contains a file entry with the following structure:
- Each of the enties start with 4 bytes `FiLE`.
- Following 32 bytes describe the 'long name' of the file; and contains a null-terminated string.
- Next 12 bytes contain a short name for the file. This is also a null-terminated string (practically, this and the long name are identical for **every** file in all archive files in the game).
- Bytes 53 to 65 of every entry contains 3 little-endian integers: offset of the file; length of the file. The third integer is always equal to the second for some reason.

It also seems possible to parse the archive file without adhering to the file table at the end; as each file seems to be prepended by the same 65-byte entry in the file. But I haven't tried it.

I wrote a hacky Python3 script (`extractCKD.py`) to *glob* the `ckd/` folder and extract each CKD archive into a corresponding folder. Usage is as follows:

```
cd res/
python extractCKD.py
```

Currently I am working on a crappy Windows machine at home, and I didn't have a chance to test these scripts in Linux; so beware.

| .CKD file | Extracted to |
| ------ | ------ |
| ckd/0GFL.CKD | extracted_0gfl_ckd/orig/ |
| ckd/3D.CKD | extracted_3d_ckd/orig/ |
| ckd/FRPOS.CKD | extracted_frpos_ckd/orig/ |
| ckd/IKON.CKD | extracted_ikon_ckd/orig/ |
| ckd/IVIR.CKD | extracted_ivir_ckd/orig/ |
| ckd/MUZIK.CKD | extracted_muzik_ckd/orig/ |

### .CMP and .PAL files

`.CMP` files are indexed-color image files with compression; and `.PAL` files are RGB color palettes.

The `.PAL` files simply contain red, green, blue values for each indexed color, encoded as [`R0`, `G0`, `B0`, `R1`, `G1`, `B1`, ...]. `.CMP` files need a `.PAL` file to be correctly interpreted; because they encode the indexes in the palette instead of the colors themselves. One interesting aspect of `.PAL` files is that the value range is not [0, 256); it is [0, 64): To obtain an 256-color palette; multiply the values by 4.

`.CMP` files are compressed bitmaps; with a very simple compression methodology (Although I have to admit I spent some time figuring it out):
- Bytes [0, 2) and [2, 4) contain the width and height of the image respectively, encoded as little-endian unsigned shorts.
- The next `w * h` bytes (rest of the file, typically) encode index values for each pixel left-to-right, top-down; to be interpreted as such:
    - If the byte value `B < 0x80`; then the next `B` bytes will represent the next `B` pixels (each will be an index from the palette).
    - If `B >= 0x80`, then repeat the next `1` byte will represent `B - 0x80` pixels (repeated value).

Here is an example:
- Let the `.PAL` file be `00 00 3f` `3f 00 00` `3f 3f 00`; so P = [blue, red, yellow].
- Let the `.CMP` file be `03 00` `04 00` `84 00` `04 01 00 02 01` `84 02`.
- Width and height are `03 00` and `04 00`; so the image is 3 x 4.
- Pixels are encoded as follows:
    - `84` means _4 times the next byte_, so 4 times `00`.
    - `04` means _the next 4 bytes are as-is_, so `01 00 02 01` values are copied.
    - `84` means _4 times the next byte_, so 4 times `02`.
    - The final decompressed bitmap is `00 00 00 00` `01 00 02 01` `02 02 02 02`.
    - Reshaped into a 3*4 matrix; the image is `[[0 0 0], [0 1 0], [2 1 2], [2 2 2]]`.
- At this point; use the palette to decode colors; which are `[[B B B], [B R B], [Y R Y], [Y Y Y]]`.
- Here is the decoded 3*4 image:
![](http://oi68.tinypic.com/2vty15s.jpg)

`convertCMP.py` in the `res/` folder can be used for converting `.CMP` files to `.PNG` images. Usage:
```
python convertCMP.py fileName [paletteName]
```
If `paletteName` is not provided; the script will attempt to find a `.PAL` file with the same name in the same directory.

__

If we apply the script to `ACILISA.CMP` and `ACILISA.PAL` files (extracted from `IVIR.CKD` using `extractCKD.py`), we obtain this picture; which is the game's main menu background:

![](http://oi66.tinypic.com/eojlf.jpg)

Shiny!


### .SND and .PUR files

_Coming_