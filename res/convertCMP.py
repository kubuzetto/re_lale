import os
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from math import floor
import numpy as np
import struct
import string
from PIL import Image
import glob

def readPalette (filepath):
	palette = []
	with open(filepath, 'rb') as f:
		d = f.read ()
		palette = [struct.unpack ('>BBB', d[(3*i):(3*i+3)]) for i in range(int(len(d)/3))]
	return [(p[0]*4,p[1]*4,p[2]*4) for p in palette]

def drawcmp (filepath, defpalpath):
	palpath = filepath.replace (".CMP", ".PAL")
	try:
		palette = readPalette (palpath)
	except:
		palette = readPalette (defpalpath)
	with open(filepath, 'rb') as f:
		d = f.read ()
		(w, h) = struct.unpack ('<HH', d[0:4])
		D = bytes(d[4:])
		WW = []
		W = np.zeros ((w, h, 3), dtype=np.uint8)
		x = 0
		y = 0
		P = 0
		N = 1
		for t in D:
			if P == 0:
				if t > 0x80:
					N = t - 0x80
					P = 1
				else:
					N = 1
					P = t
			else:
				for q in range (N):
					if x >= w or y >= h:
						WW.append (W)
						W = np.zeros ((w, h, 3), dtype=np.uint8)
						x = 0
						y = 0
					W[x, y] = palette[t % len(palette)]
					x = x + 1
					if (x >= w):
						x = 0
						y = y + 1
				P = P - 1
		if (x > 1 or y > 0):
			WW.append (W)
		
		y = 0
		for w in WW:
			im = Image.fromarray (np.transpose (w, (1, 0, 2)))
			fp = filepath.replace (".CMP", "")
			if (y > 0):
				fp = fp + "_seq" + str(y)
			im.save (fp + ".PNG")
			y = y + 1

def main ():
	if (len(sys.argv) < 2):
		print ("Usage: convertCMP fileName [paletteName]")
		return
	FN = sys.argv[1]
	pn = None
	if (len(sys.argv) > 2):
		pn = sys.argv[2]
	
#	for name in glob.glob(FN + '\\*.CMP'):
#		drawcmp (name, pn)
	drawcmp (FN, pn)

if __name__ == "__main__":
	main ()