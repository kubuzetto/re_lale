import os
import sys
import struct
import glob

def ckdread (filename):
	filepath = 'ckd/' + filename + '.CKD';
	ffold = filename.lower()
	fpath = 'extracted_' + ffold + '_ckd/orig/'
	os.makedirs(fpath)
	print('Extracting ' + filepath + ' into ' + fpath)
	with open(filepath, 'rb') as f:
		d = f.read ()
		if not d[0:4] == b'FLiB':
			print ('Invalid magic number at indices 0..4')
			return
		if not d[12:16] == b'FiLE':
			print ('Invalid magic number at indices 12..16')
			return
		dirIdx = struct.unpack ('<I', d[8:12])[0]
		i = 0
		while (True):
			pos = dirIdx + 65 * i
			i = i + 1
			if (pos >= len (d)):
				break
			if not d[pos:pos+4] == b'FiLE':
				continue
			flongname = d[(pos+4):(pos+36)].decode().split('\0')[0]
			fshrtname = d[(pos+36):(pos+48)].decode().split('\0')[0]
			(a, b, c) = struct.unpack ('<III', d[(pos+53):(pos+65)])
			fdat = d[a:(a+b)]
			
			with open (fpath + flongname, 'wb') as ff:
				ff.write (fdat)
			
			print (str(i) + '  ' + flongname + '   \t' + fshrtname + '\t   ' + str(a) + ', ' + str(b) + ', ' + str(c))
		print ("fileSize is " +str(len(d)) + ", dir offset is " + str(dirIdx))
		
def main ():
	for name in glob.glob('ckd/*.CKD'):
		filename = name.split('/')[-1].split('\\')[-1].split('.')[0]
		ckdread(filename)

if __name__ == "__main__":
	main ()
