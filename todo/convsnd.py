import os
import sys
import struct
import numpy as np
import wave
import glob

def readPUR (filepath):
	with open(filepath, 'rb') as f:
		return np.array (f.read())

def writeWav (samples, path):
	with wave.open(path, 'wb') as w:
		# 22050 Hz for SND files
		w.setparams((1, 1, 44100, 1, 'NONE', 'not compressed'))
		w.writeframes (samples)

def convPUR (filepath):
	data = readPUR (filepath)
	writeWav(data, filepath.replace ('.PUR', '') + '.wav')

def main ():
	if (len(sys.argv) < 2):
		print ("Usage: convsnd folderName")
		return
	FN = sys.argv[1]
	for name in glob.glob(FN + '\\*.PUR'):
		convPUR (name)

if __name__ == "__main__":
	main ()
