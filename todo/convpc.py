import os
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from math import floor
import numpy as np
import struct
import string
from PIL import Image
import glob

def drawcmp (filepath):
	with open(filepath, 'rb') as f:
		d = list(f.read ())
		D = d[20:]
		
		D = d[43:] # dunno
		h = 45 # 3 rows
		w = int((len (D) + h - 1) / h)
		D.extend([0]*(w * h - len(D)))
		M = np.array (D)
		M = M.reshape (w, h)
		plt.imshow(M);
		plt.show()

def main ():
	if (len(sys.argv) < 2):
		print ("Usage: convpc fileName")
		return
	FN = sys.argv[1]
	drawcmp (FN)

if __name__ == "__main__":
	main ()
